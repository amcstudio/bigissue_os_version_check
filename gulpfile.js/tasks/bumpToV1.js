var gulp = require('gulp'),
bump = require('gulp-bump');

module.exports = function (done){
 
    gulp.src('./package.json')
        .pipe(bump({
        version: '1.0.0'
        }))
        .pipe(gulp.dest('./'));
        done();
}