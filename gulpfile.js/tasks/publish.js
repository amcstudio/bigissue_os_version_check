var gulp = require('gulp'), 
    fs = require('fs'),
    zip = require('gulp-zip'),
    path = require('path'),
    filePath = path.basename(path.dirname(path.dirname(__dirname))),
    json;

    module.exports = function () {
        json = JSON.parse(fs.readFileSync('./package.json'))
        var currentVersion = json.version;
        currentVersion = currentVersion.substr(0, currentVersion.lastIndexOf('.'));
        return gulp.src('./deploy/**/*', {
            base: './deploy'
        })

        .pipe(zip(filePath + "_V" + currentVersion + "_publish.zip"))

        .pipe(gulp.dest('./_publishZip'));
    }